# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "In a rails controller", :rails, type: :controller do
  controller do
    include Gitlab::Experiment::Dsl

    def not_signed_in
      render json: experiment_result(actor: nil)
    end

    def signed_in
      render json: experiment_result(actor: 42)
    end

    def url_helpers
      e = experiment(:example).tap { |e| e.context.key('abc123') }

      render json: {
        direct_path: experiment_redirect_path(e, url: 'https://example.com'),
        direct_url: experiment_redirect_url(e, url: 'https://example.com'),
        resource_path: experiment_engine.experiment_path(e)
      }
    end

    def experiment_result(context = {})
      experiment(:example, **context) do |e|
        e.control { { value: e.context.value, signature: e.signature } }
        e.run # calling run will cause a return of the result instead of the instance
      end
    end
  end

  let(:cookie_name) { 'gitlab_experiment_example_id' }

  before do
    allow(SecureRandom).to receive(:uuid).and_return('abc123-456')

    routes.draw do
      get :not_signed_in, to: 'anonymous#not_signed_in'
      get :signed_in, to: 'anonymous#signed_in'
      get :url_helpers, to: 'anonymous#url_helpers'
    end
  end

  it "sets a cookie value and uses it for future resolution" do
    get :not_signed_in

    expect(cookies.signed[cookie_name]).to eq('abc123-456')
    expect(JSON.parse(response.body, symbolize_names: true)).to eq(
      value: { actor: 'abc123-456' },
      signature: {
        key: key_for(:example, actor: 'abc123-456'),
        variant: 'control',
        experiment: 'gitlab_experiment_example'
      }
    )
  end

  it "uses the existing cookie for resolution" do
    cookies.signed[cookie_name] = 'xyz123-098'

    get :not_signed_in, format: :json

    expect(JSON.parse(response.body, symbolize_names: true)).to eq(
      value: { actor: 'xyz123-098' },
      signature: {
        key: key_for(:example, actor: 'xyz123-098'),
        variant: 'control',
        experiment: 'gitlab_experiment_example'
      }
    )
  end

  it "migrates from the cookie value to the provided user id" do
    cookies.signed[cookie_name] = 'xyz123-098'

    get :signed_in

    expect(JSON.parse(response.body, symbolize_names: true)).to eq(
      value: { actor: 42 },
      signature: {
        key: key_for(:example, actor: '42'),
        migration_keys: [key_for(:example, actor: 'xyz123-098')],
        variant: 'control',
        experiment: 'gitlab_experiment_example'
      }
    )
  end

  it "generates the expected url helpers for rails" do
    get :url_helpers

    expect(JSON.parse(response.body, symbolize_names: true)).to eq(
      direct_path: '/glex/gitlab_experiment_example:abc123?https://example.com',
      direct_url: 'http://localhost:3000/glex/gitlab_experiment_example:abc123?https://example.com',
      resource_path: '/glex/gitlab_experiment_example:abc123'
    )
  end
end
