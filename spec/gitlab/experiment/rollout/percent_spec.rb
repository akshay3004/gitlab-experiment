# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Rollout::Percent do
  let(:subject_experiment) { Gitlab::Experiment.new(:example) }
  let(:counts) { Hash.new(0) }

  before do
    stub_experiments(example: true)

    subject_experiment.variant(:variant1) {}
    subject_experiment.variant(:variant2) {}
  end

  describe "validation" do
    it "validates array type distribution rules with the behaviors count" do
      subject_experiment.rollout(described_class, distribution: [32, 25, 43])

      expect { subject_experiment.assigned.name }.to raise_error(
        Gitlab::Experiment::InvalidRolloutRules,
        "the distribution rules don't match the number of behaviors defined"
      )
    end

    it "validates hash type distribution rules with the variant count" do
      subject_experiment.rollout(described_class, distribution: { variant1: 100 })

      expect { subject_experiment.assigned.name }.to raise_error(
        Gitlab::Experiment::InvalidRolloutRules,
        "the distribution rules don't match the number of behaviors defined"
      )
    end

    it "validates that the distribution rules are a known type" do
      subject_experiment.rollout(described_class, distribution: :foo)

      expect { subject_experiment.assigned.name }.to raise_error(
        Gitlab::Experiment::InvalidRolloutRules,
        'unknown distribution options type'
      )
    end

    it "validates the total is 100%"
  end

  context "with default distribution" do
    before do
      subject_experiment.rollout(described_class)
    end

    it "handles when there are no behaviors" do
      allow(subject_experiment).to receive(:variant_names).and_return([])

      expect { run_cycle(subject_experiment) }.not_to raise_error
    end

    it "rolls out relatively evenly to 2 behaviors" do
      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant1: 45, variant2: 55)
    end

    it "rolls out relatively evenly to 3 behaviors" do
      subject_experiment.variant(:variant3) {}

      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant1: 34, variant2: 37, variant3: 29)
    end
  end

  context "when distribution is specified as an array" do
    before do
      subject_experiment.rollout(described_class, distribution: [32, 25, 43])
    end

    it "rolls out with the expected distribution" do
      subject_experiment.variant(:variant3) {}

      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant1: 26, variant2: 27, variant3: 47)
    end
  end

  context "when distribution is specified as a hash" do
    before do
      subject_experiment.rollout(described_class, distribution: { variant1: 90, variant2: 10 })
    end

    it "rolls out with the expected distribution" do
      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant1: 90, variant2: 10)
    end
  end

  def run_cycle(experiment, **context)
    experiment.reset!(context)

    counts[experiment.assigned.name.to_sym] += 1
  end
end
