# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Configuration do
  it "responds to the expected getter/setter interface" do
    expect((described_class.methods - Object.methods).sort).to eq [
      :name_prefix, :name_prefix=,
      :logger, :logger=,
      :base_class, :base_class=,
      :strict_registration, :strict_registration=,
      :cache, :cache=,
      :default_rollout, :default_rollout=,
      :context_key_secret, :context_key_secret=,
      :context_key_bit_length, :context_key_bit_length=,
      :mount_at, :mount_at=,

      :redirect_url_validator, :redirect_url_validator=,
      :tracking_behavior, :tracking_behavior=,
      :nested_behavior, :nested_behavior=,
      :publishing_behavior, :publishing_behavior=,
      :cookie_domain, :cookie_domain=,

      :instance, :_load, # singleton assurances
      :deprecated # internal helper methods
    ].sort
  end

  describe ".default_rollout" do
    around do |example|
      original_rollout = config.default_rollout
      example.run
      config.default_rollout = original_rollout
    end

    describe "when specifying various arguments" do
      using RSpec::Parameterized::TableSyntax
      where(:rollout, :options, :expected_klass_and_options) do
        :random                             | nil            | [Gitlab::Experiment::Rollout::Random, {}]
        :percent                            | { foo: 'bar' } | [Gitlab::Experiment::Rollout::Percent, { foo: 'bar' }]
        'gitlab/experiment/rollout/random'  | nil            | [Gitlab::Experiment::Rollout::Random, {}]
        Gitlab::Experiment::Rollout::Random | { foo: 'bar' } | [Gitlab::Experiment::Rollout::Random, { foo: 'bar' }]
      end

      with_them do
        it "resolves the strategy" do
          config.default_rollout = rollout, options

          result = config.default_rollout
          expect(result).to be_a(Gitlab::Experiment::Rollout::Strategy)
          expect(result.klass).to be(expected_klass_and_options[0])
          expect(result.options).to eq(expected_klass_and_options[1])
        end
      end
    end

    context "when specifying a rollout strategy" do
      it "resolves to the correct strategy" do
        config.default_rollout = Gitlab::Experiment::Rollout::Strategy.new(
          Gitlab::Experiment::Rollout::Percent,
          foo: 'bar'
        )

        result = config.default_rollout
        expect(result).to be_a(Gitlab::Experiment::Rollout::Strategy)
        expect(result.klass).to be(Gitlab::Experiment::Rollout::Percent)
        expect(result.options).to eq(foo: 'bar')
      end
    end

    context "when dealing with deprecation warnings" do
      it "allows specifying a method name, and warning message" do
        expect_any_instance_of(ActiveSupport::Deprecation).to receive('warn').with(
          # When we specify 22.2.2, the next version will be 22.3.
          '`old_method` is deprecated and will be removed from Gitlab::Experiment 22.3 (instead use `new_method`)',
          an_instance_of(Array)
        )

        config.deprecated(:old_method, 'instead use `new_method`', version: '22.2.2')

        expect(config.instance_variable_get(:@__dep_versions).keys).to include '22.3'
      end

      it "allows providing our own warning message that includes the deprecation release info" do
        expect_any_instance_of(ActiveSupport::Deprecation).to receive('warn').with(
          # When we only specify 22.2 (without a patch version), the next version will be 23.0.
          '`old_method` will be removed from Gitlab::Experiment 23',
          anything
        )

        config.deprecated('`old_method` will be removed from {{release}}', version: '22.2')
      end
    end
  end
end
