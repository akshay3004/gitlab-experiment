# frozen_string_literal: true

RSpec.configure do |config|
  config.around(:example, suppress_warnings: true) do |example|
    config.raise_on_warning = false
    old_verbose = $VERBOSE
    $VERBOSE = nil
    example.run
  ensure
    $VERBOSE = old_verbose
    config.raise_on_warning = true
  end
end
