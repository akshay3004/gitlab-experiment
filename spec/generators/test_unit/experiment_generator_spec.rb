# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "TestUnit::Generators::ExperimentGenerator", :rails, type: :generator do
  tests { TestUnit::Generators::ExperimentGenerator }
  destination { Rails.root.join('generated') }
  let(:args) { %w[NullHypothesis foo bar baz] }

  it "generates the test unit file" do
    run_generator(args)

    expect(destination_root).to have_structure {
      file('test/experiments/null_hypothesis_experiment_test.rb') do
        contains <<~ERB
          # frozen_string_literal: true

          require 'test_helper'
          
          class NullHypothesisExperimentTest < ActiveSupport::TestCase
            # test "the truth" do
            #   assert true
            # end
          end
        ERB
      end
    }
  end
end
