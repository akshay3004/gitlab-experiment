# frozen_string_literal: true

module Release
  module CommonActions
    def self.included(base)
      super(base)

      base.extend(
        Thor::Actions::ClassMethods,
        ClassMethods
      )

      base.include(
        Thor::Actions,
        UrlHelpers,
        GitHelpers,
        GemHelpers,
        ChangelogHelpers,
        GitlabHelpers,
        TempPathHelpers,
        CommandHelpers
      )
    end

    module ClassMethods
      def exit_on_failure?
        false
      end

      def require_commands(names)
        @_command_names = names
      end

      def start(*args)
        (@_command_names || []).each do |name|
          require_relative "commands/#{name}_command"
        end

        super
      end
    end

    def invoke_command(command, *args)
      super
    rescue Interrupt
      say ""
      say_status(:quit, 'ok, exiting', :red)
    rescue RuntimeError => e
      say_status(:failure, e.message.to_s.split("\n").join("\n#{' ' * 14}"), :red)
      exit(1) # rubocop:disable Rails/Exit
    end

    def version(prefix: false)
      version = (options[:version] || gemspec.version).to_s.gsub(/^v/i, '')
      prefix ? "v#{version}" : version
    end

    def yes?(message, fail: nil)
      return true if options[:force]

      result = super("#{message.strip} [yN]")
      fail fail if !result && fail

      result
    end

    def exists?(filename)
      File.exist?(File.expand_path(filename, destination_root))
    end

    module UrlHelpers
      def base_gitlab_url(*parts)
        "https://gitlab.com/#{parts.join('/')}"
      end

      def gitlab_path
        'gitlab-org/gitlab'
      end

      def project_url(*parts)
        base_gitlab_url(*[project_path, *parts])
      end

      def gitlab_url(*parts)
        base_gitlab_url(*[gitlab_path, *parts])
      end

      def artifact_url
        id = CGI.escape(project_path)
        base_gitlab_url('api/v4/projects', id, 'packages/generic/gem-builds', version, gem_filename)
      end
    end

    module GitHelpers
      def git(command, **config, &block)
        run_command(:git, command, **config, &block)
      end

      def current_branch
        git('symbolic-ref --short HEAD', verbose: false).to_s.strip
      end

      def latest_tagname
        git('tag --sort -version:refname | head -n 1', verbose: false).to_s.strip
      end

      def any_unstaged_files?
        git('update-index --refresh || echo "yes"', verbose: false).to_s.strip == 'yes' ||
          git('diff-index --quiet HEAD -- || echo "yes"', verbose: false).to_s.strip == 'yes'
      end

      def any_unpushed_changes?
        !git(%(log origin/#{current_branch}..#{current_branch} || echo "yes"), verbose: false).to_s.strip.empty?
      end

      def current_sha
        git('rev-parse HEAD', verbose: false).to_s.strip
      end
    end

    module GemHelpers
      def gemspec
        self.class.gemspec
      end

      def gem(command, **config, &block)
        run_command(:gem, command, **config, &block)
      end

      def gemfile_line(url: nil)
        if url
          "gem '#{gemspec.name}', git: '#{url}.git', branch: '#{release_branch}'"
        else
          "gem '#{gemspec.name}', '~> #{version}'"
        end
      end

      def gem_filename
        "#{gemspec.name}-#{version}.gem"
      end
    end

    module ChangelogHelpers
      def changelog_content
        File.read(File.expand_path(File.join('../../', changelog_file), __dir__))
      rescue Errno::ENOENT
        "No changelog for this release"
      end

      def changelog_entries
        return [] if options[:force]

        say "Provide changelog entries, using a blank entry to finish."

        entries = []
        until (entry = ask("- :")).empty?
          entries.push(entry)
        end

        entries
      end
    end

    module GitlabHelpers
      def gitlab
        @gitlab ||= begin
          Gitlab.configure do |config|
            config.define_singleton_method(:curl_token) do
              "#{ENV['GITLAB_API_PRIVATE_TOKEN'] ? 'PRIVATE' : 'JOB'}-TOKEN: #{config.private_token}"
            end

            config.reset

            config.endpoint ||= base_gitlab_url('api/v4')
            config.private_token ||= ENV['CI_JOB_TOKEN'] || ask('Enter your GitLab API token to continue:')
            config.httparty = {
              debug_output: options[:quiet] == false ? $stdout : false
            }
          end

          Gitlab
        end
      end

      def with_gitlab(action_description)
        return unless behavior == :invoke

        say_status :gitlab, action_description
        return if options[:pretend]

        yield(gitlab)
      end

      def existing_merge_request(path, branch, state: nil)
        results = gitlab.merge_requests(path, source_branch: branch, state: state)
        return false if results.empty?

        results.first
      end

      def create_merge_request(path, branch, title:, description:, extra: nil, labels: [], **config)
        return unless behavior == :invoke

        say_status :gitlab, "opening merge request on #{path} for #{branch}"
        return if options[:pretend]

        description = "#{description}\n\n-----\n\n\n#{extra}" if extra

        merge_request = gitlab.create_merge_request(
          path, title,
          config.merge(
            source_branch: branch,
            assignee_id: gitlab.user.id,
            description: description,
            labels: labels.join(', ')
          )
        )

        say_status :ok, "merge request at #{merge_request.web_url}"

        merge_request
      end

      def update_merge_request(merge_request, extra: nil, **config)
        return unless behavior == :invoke

        say_status :gitlab, "updating merge request for #{merge_request.source_branch}"
        return if options[:pretend]

        description = merge_request.description
        description = description.gsub(/\n\n-----\n\n\n(.*)/m, "\n\n-----\n\n\n#{extra}") if extra

        gitlab.update_merge_request(
          merge_request.project_id,
          merge_request.iid,
          config.merge(
            description: description,
            title: merge_request.title
          )
        )

        say_status :ok, "merge request at #{merge_request.web_url}"

        merge_request
      end

      def create_gemfile_commit(with_line, from_branch: default_branch)
        within_temp_path do |path|
          # minimally clone gitlab from the branch we want to create our new branch from
          git "clone --depth=1 --branch=#{from_branch} #{gitlab_url}.git ."

          # update the gemfile with the new gem line
          gsub_file 'Gemfile', /gem ['"]#{gemspec.name}['"].*/o, with_line

          # do a minimal bundle process to only update the gem, for the gemfile inside gitlab
          run(
            "bundle lock --update=#{gemspec.name} --gemfile=./Gemfile --lockfile=./Gemfile.lock",
            capture: options[:quiet] != false
          )

          run(
            "git --no-pager diff",
            capture: options[:quiet] != false
          )

          # create the commit, using the api
          actions = [
            { action: :update, file_path: 'Gemfile', content: File.read(File.join(path, 'Gemfile')) },
            { action: :update, file_path: 'Gemfile.lock', content: File.read(File.join(path, 'Gemfile.lock')) }
          ]
          create_commit(gemfile_commit_message, gitlab_path, gitlab_release_branch, from_branch, actions)
        end
      end

      def create_commit(title, path, branch, from_branch, actions)
        return unless behavior == :invoke

        say_status :gitlab, "committing update on #{path}"
        return if options[:pretend]

        params = { author_name: gitlab.user.name }
        params[:start_branch] = from_branch if from_branch == default_branch
        gitlab.create_commit(path, branch, title, actions, params)
      rescue Gitlab::Error::BadRequest => e
        raise e unless e.response_message.include?('already exists')
        message = "It looks like the #{branch} branch already exists, commit to it?"
        if yes?(message)
          create_commit(title, path, branch, branch, actions)
        else
          say_status :ok, 'not applying any commits'
        end
      end
    end

    module TempPathHelpers
      def within_temp_path(&block)
        inside('tmp', &block)
      ensure
        say_status :ok, 'cleaning up tmp directory'
        FileUtils.rm_r('tmp', force: true, verbose: false)
      end
    end

    module CommandHelpers
      def run_command(action, command, require_success: true, verbose: true, env: nil)
        return unless behavior == :invoke

        verbose = true if options[:quiet] == false

        destination = relative_to_original_destination_root(destination_root, false)
        say_status(action, "#{command} from #{destination.inspect}", verbose)
        return if options[:pretend]

        result, status = Open3.capture2e(*Array(env), "#{action} #{command}")
        success = status.success?

        fail result if require_success && !success

        if block_given?
          yield success, result
        else
          abort unless success
        end

        result
      end
    end
  end

  class CLI < Thor
    include CommonActions
  end
end
