### 0.7.1 Changelog

- Fixes some concurrency issues with rollout strategies
- Fixes issue with [before/around/after]_run callbacks
