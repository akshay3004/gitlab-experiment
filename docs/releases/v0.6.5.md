### 0.6.5 Changelog

- Fixes a bug where nil behaviors could allow multiple runs
- Introduces new `sticky_to` option for better tracking while minimizing cache size
- Adds nested experiment implementation that enables experiment diagramming
