# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab/experiment/version'

Gem::Specification.new do |spec|
  spec.name = 'gitlab-experiment'
  spec.version = Gitlab::Experiment::VERSION
  spec.authors = ['GitLab']
  spec.email = ['gitlab_rubygems@gitlab.com']
  spec.summary = 'GitLab experimentation library.'
  spec.homepage = 'https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment'
  spec.license = 'MIT'
  spec.files = Dir['lib/{generators,gitlab}/**/*'] + ['LICENSE.txt', 'README.md']
  spec.required_ruby_version = '>= 2.6'

  spec.add_runtime_dependency 'activesupport', '>= 3.0'
  spec.add_runtime_dependency 'request_store', '>= 1.0'
end
